hey (0.1.4+ds-1) unstable; urgency=medium

  * New upstream version 0.1.4+ds
  * d/control:
      - Add help2man to Build-Depends
      - Add golang-golang-x-text-dev to Build-Depends
      - Bump Standards-Version to 4.7.0 (no changes)
      - Move golang-any from Build-Depends-Arch to Build-Depends
      - Replace dh-golang with dh-sequence-golang
  * d/copyright:
      - Add vendor folder to Files-Excluded
      - Remove vendor folder license declaration
      - Update Arthur Diniz copyright year to 2024
  * d/gbp.conf: Created
  * d/hey.1: Remove static manpage
  * d/manpage.sh: Remove script to generate manpage
  * d/rules:
      - Add execute_before_dh_installman to generate manpage with help2man
      - Add --builddirectory=_build flag to dh entry
      - Remove --with=golang flag from dh entry
  * d/u/metadata: Add upstream Repository and Repository-Browse
  * d/watch:
      - Add repacksuffix and dversionmangle
      - Add compression flag set to gz
  * d/copyright:
      - Remove BSD-3-Clause license paragraph
      - Remove vendor folder license declaration
      - Update Arthur Diniz copyright year to 2024

 -- Arthur Diniz <arthurbdiniz@gmail.com>  Fri, 14 Jun 2024 03:42:10 +0100

hey (0.1.4-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 15:59:31 +0000

hey (0.1.4-1) unstable; urgency=medium

  * New upstream version 0.1.4
  * Bump debhelper compatibility level to 13
  * debian/control:
      - Add golang-golang-x-net-dev as build dependency
      - Declare compliance with debian policy 4.5.1
      - Set Rules-Requires-Root to no
  * debian/copyright:
      - Update information about upstream authors
      - Update the packaging copyright years
  * debian/manpage.sh: Exclude SEE-ALSO section from manpage (Closes: #959932)
  * debian/hey.1: Regenerate manpage removing SEE-ALSO section

 -- Arthur Diniz <arthurbdiniz@gmail.com>  Fri, 11 Dec 2020 16:17:24 -0300

hey (0.1.2-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/rules: move override_dh_auto_install below main entry.

 -- Arthur Diniz <arthurbdiniz@gmail.com>  Mon, 11 Nov 2019 23:46:00 -0300

hey (0.1.2-1) experimental; urgency=medium

  * Initial release (Closes: #941983)

 -- Arthur Diniz <arthurbdiniz@gmail.com>  Thu, 31 Oct 2019 15:12:12 -0400
